package com.example.controllerprogect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControllerProgectApplication {

    public static void main(String[] args) {
        SpringApplication.run(ControllerProgectApplication.class, args);
    }

}
