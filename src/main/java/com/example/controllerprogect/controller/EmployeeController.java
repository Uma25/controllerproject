package com.example.controllerprogect.controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
@RestController
public class EmployeeController {
    List listPerson = new ArrayList();

    @GetMapping("/person")
    public String getPerson(@RequestParam String name,
                            @RequestParam String surname,
                            @RequestParam String patronymic,
                            @RequestParam String gmail,
                            @RequestParam Long phoneNumber) {
        listPerson.add(name + " " +
                surname + " " +
                patronymic + " " +
                gmail + " " +
                phoneNumber);
        return name + " " +
                surname + " " +
                patronymic + " " +
                gmail + " " +
                phoneNumber;
    }

    @GetMapping("/persons")
    public String getPersonAll() {
        return String.valueOf(listPerson);
    }

    @PutMapping("/personPut")
    public String personPut() {
        return "Update";
    }

    @PostMapping("/personPost")
    public String personPost() {
        listPerson.add("Sergey" + " " +
                "Patukov" + " " +
                "Sergeevich" + " " +
                "sergey@gmail" + " " +
                "89899999999");
        return String.valueOf(listPerson);
    }

    @DeleteMapping("/personDelete")
    public String deletePerson(@RequestParam String name,
                               @RequestParam String surname,
                               @RequestParam String patronymic,
                               @RequestParam String gmail,
                               @RequestParam Long phoneNumber) {
        listPerson.remove(name + " " +
                surname + " " +
                patronymic + " " +
                gmail + " " +
                phoneNumber);
        return String.valueOf(listPerson);
    }
}
